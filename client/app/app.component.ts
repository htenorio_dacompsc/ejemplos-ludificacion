import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as firebase from 'firebase';
import { MediaMatcher } from '@angular/cdk/layout';
import { AuthService, AlertService, UserService } from './services';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'Ejemplo componentes';
  urlImage: any;

  importacion = [
    {text: 'NGX Dropzone Wrapper', icon: '', url: 'ngxDropzoneWrapper'}
  ];

  renderizacion = [
    {text: 'Open Graph Scraper', icon: '', url: 'openGraphScraper'}
  ];

  graficos = [
    {text: 'Line Chart', icon: '', url: 'lineChart'},
    {text: 'Bar Chart', icon: '', url: 'barChart'},
    {text: 'Doughnut Chart', icon: '', url: 'doughnutChart'},
    {text: 'Radar Chart', icon: '', url: 'radarChart'},
    {text: 'Pie Chart', icon: '', url: 'pieChart'},
    {text: 'Polar Area Chart', icon: '', url: 'polarAreaChart'},
    {text: 'Dynamic Chart', icon: '', url: 'dynamicChart'},
  ];

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    public authService: AuthService, private alertService: AlertService, private userService: UserService) {

  }

  ngOnInit(): void {
    firebase.initializeApp({
      // For other projects use different keys
      apiKey: 'AIzaSyDmlqQ5ayNgSWDCuip-XQtz1tHaYoix7ks',
      authDomain: 'mtie-c0ef4.firebaseapp.com',
      databaseURL: 'https://mtie-c0ef4.firebaseio.com',
      projectId: 'mtie-c0ef4',
      storageBucket: 'mtie-c0ef4.appspot.com',
      messagingSenderId: '1066309040052'
    });

  }

  public userUid() {
    this.userService.getUserProfileInformation();
    return firebase.auth().currentUser.uid;
  }

  public userEmail() {
    this.userService.getUserProfileInformation();
    return firebase.auth().currentUser.email;
  }

  public userName() {
    this.userService.getUserProfileInformation();
    return firebase.auth().currentUser.displayName;
  }

  public photoURL() {
    this.userService.getUserProfileInformation();
    return firebase.auth().currentUser.photoURL;
  }

  public onLogout() {
    this.authService.logout();
    this.alertService.showToaster('Salida exitosa');
  }

}

// Para que el front-end (cliente) este escuchando el back-end (server) debe estar corriendo el servidor:
  // nodemon server.js --watch server
// Y la vista (cliente) debe estar corriendo con el archivo proxy:
  // ng serve --proxy-config proxy.config.json --open
