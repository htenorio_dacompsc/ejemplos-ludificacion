import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,
         Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/aniMations';
import { ChartsModule } from 'ng2-charts';
import { Http,
         HttpModule,
         BaseRequestOptions,
         RequestOptions,
         RequestOptionsArgs } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';

import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { MatToolbarModule,
         MatCardModule,
         MatListModule,
         MatButtonModule,
         MatButtonToggleModule,
         MatSnackBarModule,
         MatMenuModule,
         MatGridListModule,
         MatInputModule,
         MatProgressBarModule,
         MatIconModule,
         MatTableModule,
         MatPaginatorModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule,
         ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule,
         FileUploader } from 'ng2-file-upload';

import { UserService,
         AlertService,
         AuthGuardService,
         AuthService } from './services';

import { CookieService } from 'ngx-cookie-service';
import { Ng2FileInputModule } from 'ng2-file-input';

// Inicia - Imposrtación de datos - ngx-dropzone-wrapper
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
   url: 'https://httpbin.org/post',
   maxFilesize: 50,
   acceptedFiles: '.csv'
 };
// Termina - Importación de datos - ngx-dropzone-wrapper

import { TruncatePipe } from './pipes/truncate.pipe';
import { LineChartComponent } from './lineChart/lineChart.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { DoughnutChartComponent } from './doughnut-chart/doughnut-chart.component';
import { RadarChartComponent } from './radar-chart/radar-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { PolarAreaChartComponent } from './polar-area-chart/polar-area-chart.component';
import { DynamicChartComponent } from './dynamic-chart/dynamic-chart.component';
import { OpenGraphScraperComponent } from './open-graph-scraper/open-graph-scraper.component';
import { SigninComponent } from './auth/signin.component';
import { DropzoneWrapper } from './dropzone-wrapper/dropzone-wrapper.component';

@NgModule({
declarations: [
  AppComponent,
  HomeComponent,
  TruncatePipe,
  LineChartComponent,
  BarChartComponent,
  DoughnutChartComponent,
  RadarChartComponent,
  PieChartComponent,
  PolarAreaChartComponent,
  DynamicChartComponent,
  OpenGraphScraperComponent,
  SigninComponent,
  DropzoneWrapper
],
imports: [
  AppRoutingModule,
  HttpModule,
  HttpClientModule,
  BrowserModule,
  MatToolbarModule,
  MatCardModule,
  MatListModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatMenuModule,
  MatGridListModule,
  BrowserAnimationsModule,
  ChartsModule,
  FlexLayoutModule,
  MatInputModule,
  FormsModule,
  ReactiveFormsModule,
  MatProgressBarModule,
  MatIconModule,
  Ng2FileInputModule.forRoot(),
  FileUploadModule,
  MatTableModule,
  MatPaginatorModule,
  DropzoneModule
],
providers: [
  UserService,
  AlertService,
  AuthGuardService,
  AuthService,
  CookieService,
  { // Inicia - Imposrtación de datos - ngx-dropzone-wrapper
    provide: DROPZONE_CONFIG,
    useValue: DEFAULT_DROPZONE_CONFIG
  } // Termina - Imposrtación de datos - ngx-dropzone-wrapper
],
bootstrap: [AppComponent]
})
export class AppModule { }
