// Modules 3rd party
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Main
import { AppComponent } from './app.component';

// Home page
import { HomeComponent } from './home/home.component';

// Datos gráficos
import { LineChartComponent } from './lineChart/lineChart.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { DoughnutChartComponent } from './doughnut-chart/doughnut-chart.component';
import { RadarChartComponent } from './radar-chart/radar-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { PolarAreaChartComponent } from './polar-area-chart/polar-area-chart.component';
import { DynamicChartComponent } from './dynamic-chart/dynamic-chart.component';
import { OpenGraphScraperComponent } from './open-graph-scraper/open-graph-scraper.component';
import { SigninComponent } from './auth/signin.component';
import { DropzoneWrapper } from './dropzone-wrapper/dropzone-wrapper.component';

// Routing
const appRoutes: Routes = [
  // Principal
  { path: '', redirectTo: '/home', pathMatch : 'full' },
  { path: 'home', component: HomeComponent },
  // Datos gráficos
  { path: 'lineChart', component: LineChartComponent },
  { path: 'barChart', component: BarChartComponent },
  { path: 'doughnutChart', component: DoughnutChartComponent },
  { path: 'radarChart', component: RadarChartComponent },
  { path: 'pieChart', component: PieChartComponent },
  { path: 'polarAreaChart', component: PolarAreaChartComponent },
  { path: 'dynamicChart', component: DynamicChartComponent },
  // Renderizado
  { path: 'openGraphScraper', component: OpenGraphScraperComponent },
  // Interacción con redes sociales
  { path: 'login', component: SigninComponent },
  // Importación de datos
  { path: 'ngxDropzoneWrapper', component: DropzoneWrapper },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
