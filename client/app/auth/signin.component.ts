import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AlertService, AuthService } from '../services';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  @Input() loading = false;

  constructor(private authService: AuthService, private alertService: AlertService) {
  }

  ngOnInit() {

  }

  onSignInGoogle(form: NgForm) {
    this.loading = true;
    this.authService.signInWithGoogle();
  }

  onSignInTwitter(form: NgForm) {
    this.loading = true;
    this.authService.signInWithTwitter();
  }

  onSignInFacebook(form: NgForm) {
    this.loading = true;
    this.authService.signInWithFacebook();
  }

  onSigninAnonymous(form: NgForm) {
    this.loading = true;
    this.authService.signInAnonymous();
  }

}
