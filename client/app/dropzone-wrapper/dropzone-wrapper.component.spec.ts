import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropzoneWrapper } from './dropzone-wrapper.component';

describe('DropzoneWrapper', () => {
  let component: DropzoneWrapper;
  let fixture: ComponentFixture<DropzoneWrapper>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropzoneWrapper ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropzoneWrapper);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
