import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { DropzoneComponent , DropzoneDirective, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

@Component({
  selector: 'app-dropzone-wrapper',
  templateUrl: './dropzone-wrapper.component.html',
  styleUrls: ['./dropzone-wrapper.component.css']
})
export class DropzoneWrapper implements OnInit {
  fileReaded: any;
  displayedColumns = ['col_0', 'col_1', 'col_2', 'col_3', 'col_4', 'col_5', 'col_6', 'col_7', 'col_8'];
  dataSource = new MatTableDataSource;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(DropzoneComponent) componentRef: DropzoneComponent;
  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  constructor() {

  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  convertFile(csv: any) {
    console.log('csv: ', csv);
    const _this  = this;
    if (csv.target.files.length > 0) {
      this.fileReaded = csv.target.files[0];
      const reader_1 = new FileReader();
      reader_1.readAsText(this.fileReaded);
      reader_1.onload = function (e) {
        const _csv = reader_1.result;
        const allTextLines = _csv.split(/\r|\n|\r/);
        const headers = allTextLines[0].split(',');
        const lines = [];
        for (let i = 0; i < allTextLines.length; i++) {
          // split content based on comma
          const data = allTextLines[i].split(',');
          if (data.length === headers.length) {
            const tarr = [];
            for (let j = 0; j < headers.length; j++) {
              tarr.push(data[j]);
            }
            // log each row to see output
            // console.log(tarr);
            lines.push({col_0: tarr[0], col_1: tarr[1], col_2: tarr[2], col_3: tarr[3],
              col_4: tarr[4], col_5: tarr[5], col_6: tarr[6], col_7: tarr[7], col_8: tarr[8]});
          }
        }
        // all rows in the csv file
        console.log(lines);
        _this.dataSource.data = lines;
      };
    }
  }

  convertFileDropZone(csv: any) {
    console.log('csv: ', csv);
    const allTextLines = csv[1].files.file.split(/\r|\n|\r/);
      const headers = allTextLines[0].split(',');
      const lines = [];
      for (let i = 0; i < allTextLines.length; i++) {
        // split content based on comma
        const data = allTextLines[i].split(',');
        if (data.length === headers.length) {
          const tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }
          // log each row to see output
          // console.log(tarr);
          lines.push({col_0: tarr[0], col_1: tarr[1], col_2: tarr[2], col_3: tarr[3],
            col_4: tarr[4], col_5: tarr[5], col_6: tarr[6], col_7: tarr[7], col_8: tarr[8]});
        }
      }
      // all rows in the csv file
      console.log(lines);
      this.dataSource.data = lines;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  public onUploadError(args: any): void {
    console.log('onUploadError:', args);
  }

  public resetDropzoneUploads(): void {
    this.componentRef.directiveRef.reset();
    this.dataSource.data = [];
  }

}
