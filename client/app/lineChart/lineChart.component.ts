import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-line-chart',
  templateUrl: './lineChart.component.html',
  styleUrls: ['./lineChart.component.css']
})
export class LineChartComponent implements OnInit {
  /*public lineChartData: Array<any> = [
    {data: ['65', '59', '80', '81', '56', '55', '40'], label: 'Series A'},
    {data: ['28', '48', '40', '19', '86', '27', '90'], label: 'Series B'},
    {data: ['18', '48', '77', '9', '100', '27', '40'], label: 'Series C'},
    {data: ['18', '48', '77', '9', '100', '27', '40'], label: 'SERIE C1'}
  ];*/
 public lineChartData: Array<any> = [
   {data: ['0', '0', '0', '0', '0', '0', '0'], label: 'SERIE A'},
   {data: ['0', '0', '0', '0', '0', '0', '0'], label: 'SERIE B'},
   {data: ['0', '0', '0', '0', '0', '0', '0'], label: 'SERIE C'},
   {data: ['0', '0', '0', '0', '0', '0', '0'], label: 'SERIE D'}
  ];

  public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: any = { responsive: true };
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { //
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,45,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,1)'
    }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';

  public randomize(): void {
    console.log(this.lineChartData);
    const _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
    console.log(_lineChartData);
  }

  constructor(private http: HttpClient) {
    this.getArray();
  }

  ngOnInit() {
    // this.getArray();
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  getArray(): void {
    const _this  = this;
    console.log(this.lineChartData);
    this.http.post('/api/test/getGrafica', {}).subscribe(function(result: any) {
      console.log(result.graficaArray);
      _this.lineChartData = result.graficaArray;
      /*this.lineChartData.forEach(function(element) {
        element.data.forEach(function(el) {
          console.log(el);
        });
        // console.log(element);
      });*/
    }, function(err) {
        console.log('Hubo un error con la petición: ' + err);
    });
  }

}
