import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenGraphScraperComponent } from './open-graph-scraper.component';

describe('OpenGraphScraperComponent', () => {
  let component: OpenGraphScraperComponent;
  let fixture: ComponentFixture<OpenGraphScraperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenGraphScraperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenGraphScraperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
