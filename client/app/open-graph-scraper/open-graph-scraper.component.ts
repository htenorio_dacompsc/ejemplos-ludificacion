import {AfterViewInit} from '@angular/core/src/metadata/lifecycle_hooks';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgModel } from '@angular/forms';
import { AlertService } from '../services';

@Component({
  selector: 'app-open-graph-scraper',
  templateUrl: './open-graph-scraper.component.html',
  styleUrls: ['./open-graph-scraper.component.css']
})
export class OpenGraphScraperComponent implements OnInit, AfterViewInit {
  options: any;
  ogs: any = new Object;
  url: string;
  public ogsB: boolean;
  @Input() loading = false;

  constructor(private http: HttpClient, private alertService: AlertService) {
    // this.options = {'url': 'http://www.yunius.com/nuevos-componentes-de-yunius-app-y-web'};
  }

  ngOnInit() {
    this.ogs.ogImage = new Object();
    this.ogs.ogImage.url = '';
    this.ogs.ogDescription = '';
    // this.OpenGrap(this.options);
    this.ogsB = false;
  }

  ngAfterViewInit() {
    this.ogs.ogImage = new Object;
  }

  private OpenGrap(url: string): void {
    const _this = this;
    this.loading = true;
    this.http.post('/api/test/ops', {url: url}).subscribe(function (result: any) {
      console.log(result);
      _this.ogs = result.results.data;
      _this.ogsB = result.results.success;
      _this.loading = false;
      }, function (err) {
        _this.loading = false;
        console.log(err);
        _this.alertService.showToaster('Hubo un error con la petición: ' + err);
      });
  }

}
