import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { CookieService } from 'ngx-cookie-service';

import { AlertService } from './alert.service';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  token: string;

  constructor(private router: Router, private alertService: AlertService,
    private userService: UserService, private cookieService: CookieService) {
      if (this.cookieService.check('token')) {
        // this.token = this.cookieService.get('token');
      }
      console.log(this.token);
  }

  // Signin/login
  signInWithGoogle() {
    const _this = this;
    const providerGoogle = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(providerGoogle)
      .then(function (result) {
        console.log('result', result);
        _this.token = result.credential.accessToken;
        console.log('token', _this);
        _this.cookieService.set('token', result.credential.accessToken);
        console.log(_this.cookieService.get('token'));
        const currentUser = result.user;
    })
      .then(function (response) {
      _this.router.navigate(['/']);
      firebase.auth().currentUser.getIdToken()
        .then(function (token) {
          return _this.token = token;
        });
        console.log('token', _this.token);
      _this.alertService.showToaster('Inicio de sesión de Google exitoso');
    })
      .catch(function (error) {
        return console.log(error);
      });
  }

  signInWithTwitter() {
    const _this = this;
    const providerTwitter = new firebase.auth.TwitterAuthProvider();
    firebase.auth().signInWithPopup(providerTwitter)
      .then(function (response) {
      _this.router.navigate(['/']);
      firebase.auth().currentUser.getIdToken()
        .then(function (token) {
          return _this.token = token;
        });
        console.log('token', _this.token);
      _this.alertService.showToaster('Inicio de sesión de Twitter exitoso');
    })
      .catch(function (error) {
         return console.log(error);
      });
  }

  signInWithFacebook() {
    const _this = this;
    const providerFacebook = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(providerFacebook)
      .then(function (response) {
      _this.router.navigate(['/']);
      firebase.auth().currentUser.getIdToken()
        .then(function (token) {
          return _this.token = token;
        });
        console.log('token', _this.token);
      _this.alertService.showToaster('Inicio de sesión de Facebook exitoso');
    })
      .catch(function (error) {
        return console.log(error);
      });
  }

  signInAnonymous() {
    const _this = this;
    firebase.auth().signInAnonymously()
      .then(function (response) {
      _this.router.navigate(['/']);
      firebase.auth().onAuthStateChanged(function (currentUser) {
        const isAnonymous = currentUser.isAnonymous;
        const uid = currentUser.uid;
        firebase.auth().currentUser.getIdToken()
          .then(function (token) {
            return _this.token = token;
          }),
          console.log('token', _this.token);
          _this.alertService.showToaster('Anonymous login succesful');
        console.log(currentUser);
      });
    })
      .catch(function (error) {
        return console.log(error);
      });
  }

  // Other
  logout() {
    const _this = this;
    firebase.auth().signOut()
      .then(function (response) {
      _this.router.navigate(['/']);
    })
      .catch(function (error) {
        return console.log(error);
      });
    this.token = null;
  }

  getIdToken() {
    const _this = this;
    firebase.auth().currentUser.getIdToken()
      .then(function (token) {
        return _this.token = token;
      });
    return this.token;
  }

  isAuthenticated() {
    // console.log(this.token);
    return this.token != null;
    // return this.cookieService.check('token') != null;
  }

}
