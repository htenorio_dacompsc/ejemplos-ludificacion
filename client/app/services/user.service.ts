import * as firebase from 'firebase';
import { Injectable } from '@angular/core';

// import { User, Perfil } from '../../models';
import { AlertService } from './alert.service';

@Injectable()
export class UserService {
  public displayName: string;
  public email: string;
  public bio: any;
  public image: any;
  public uid: any;
  public key: string;

  constructor(private alertService: AlertService) {
    this.key = 'AIzaSyD8qBu9hSQ41HZnxBJ8CkFNwqWYzSLfoEE';
  }

  saveUserInfo(uid, name, email
  // uid, token, image, name, username, bio, email, password
  ) {
    return firebase.database().ref().child('users/' + uid).set({
      name: name,
      email: email
    });
  }

  updateUserInfo(uid, displayName, bio) {
    return firebase.database().ref().child('users/' + uid).update({
      // token: token,
      // image: this.image,
      displayName: displayName,
      bio: bio
      // password: password
    });
  }

  getAdmin() {
    return firebase.database().ref().child('users/' + 'H1LnAAbMvIf8KhrKMMhBdcBYSK93')
    .on('value', function (snapshot) {
    });
  }

  keepInTouch(email) {
    this.alertService.showToaster('Your email is saved');
    return firebase.database().ref().child('touch/').push({
      email: email
    });
  }

  contactFormSend(company, firstname, lastname, address, city, postal, message) {
    this.alertService.showToaster('This contact form is saved');
    return firebase.database().ref().child('contactform/').push({
      company: company,
      firstname: firstname,
      lastname: lastname,
      address: address,
      city: city,
      postal: postal,
      message: message
    });
  }

  getUserProfileInformation() {
    const user = firebase.auth().currentUser;
    let name, email, photoUrl, uid, emailVerified;
    //console.log(user);
    if (user != null) {
      name = user.displayName;
      email = user.email;
      photoUrl = user.photoURL;
      emailVerified = user.emailVerified;
      uid = user.uid;
    }
  }

  verificationUserEmail() {
    firebase.auth().currentUser.sendEmailVerification().then(function () {
      // Email sent.
    }, function (error) {
        // An error happened.
    });
  }

  sendUserPasswordResetEmail() {
    firebase.auth().sendPasswordResetEmail(firebase.auth().currentUser.email).then(function () {
      // Email sent.
    }, function (error) {
        // An error happened.
    });
  }

}
