'use strict'

const express  = require('express');
const bodyParser = require('body-parser');

const app = express();
const HOSTNAME = 'localhost';
const PORT = 3000;

app.use(bodyParser.json());

const CORS = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Authorization, user, data, access_token, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
}
app.use(CORS);

require('./server/routes')(app);

app.listen(PORT, HOSTNAME, () => {
  console.log('Servidor corriendo en: ' + HOSTNAME + ' ' + PORT);
});
