'use strict'

var request = require('request');
var url = require('../../components/utils/url');

module.exports = {
  token : function(req, res, next) {
    console.log('url: ', url('oauth/token'));
    var options = {
      method: 'POST',
      url: url('oauth/token'),
      form: {
        grant_type: 'password',
        username: 'DEMO_ADMI',
        password: '20I74DM1YUN1U5D3C',
        client_id: 'YUNIUS_WEB',
        client_secret: '4yGuH7Wg456hrO2PtxfmqfMAFyukysCAKSqnMPq7p1jraH2OXxuVLYi93Uo6h9Gm'
      }
    };

    request(options, function (error, response, body) {
      try {
        //console.log('body', body);
        req.access_token = JSON.parse(body).access_token;
        next();
      } catch (error) {
        console.log('error: ', error);
        res.json({error: error});
      }
    });
  },

}
