'use strict'

const express = require('express');
const controller = require('./test.controller')
const auth = require('../auth/yunius/tokens-generator');

const router = express.Router();

router.get('/', controller.getText)
// Open Graph Scraper
router.post('/ops', controller.OpenGrapScraper);
//
router.post('/getGrafica', [auth.token], controller.GET);

module.exports = router;

