'use strict'
const ogs = require('open-graph-scraper');
var request = require('request');
var url = require('../components/utils/url');

const options = {};

module.exports = {
  getText : function(req, res) {
    console.log('getText');
    res.send({estado: 'exito'});
  },

  OpenGrapScraper : function(req, res) {
    console.log('OpenGrapScraper');
    console.log('req', req.body.url);
    var result;
    ogs(parseInput(req.body.url), function (error, results) {
      console.log('error:', error); // This is returns true or false. True if there was a error. The error it self is inside the results object.
      console.log('results:', results.data);
      res.send({results});
    });
  },

  GET : function(req, res) {
    console.log('req: ', req);
    var pBody = parseInputRequest(req.body);

    var options = {
      method: 'POST',
      url: url('api/analisis/grafica/get'),
      qs: {
        access_token: req.access_token
      },
      headers: {
        'cache-control': 'no-cache',
        'accept-language': 'application/json',
        'content-type': 'application/json'
      },
      body: pBody,
      json: true
    };

    request(options, function (error, response) {
      console.log('response: ', response)
      try {
        res.json(response.body);
      } catch (error) {
        console.log('error: ', error);
        res.json({error: error});
      }
    });
  },

}

function parseInput(url) {
  console.log('parseInputURL: ', url);
  var options = {};
  options.url = url;
  return options;
}

function parseInputRequest(body) {
  var input = {};
  input.id = {};
  input.id.cdgem = 'DEMO';
  console.log('Input: ', input);
  return input;
}
