'use strict'

function router(app) {
  app.use('/api/test', require('./api/test'));
}

module.exports = router;
